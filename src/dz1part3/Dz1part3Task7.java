package dz1part3;

import java.util.Scanner;

public class Dz1part3Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String string = input.nextLine();
        int i = 0;
        int chars = 0;
        do {
            if (string.charAt(i) != ' ') {
                chars = chars + 1;
            }
            i++;
        } while (i < string.length());
        System.out.println(chars);
    }
}
