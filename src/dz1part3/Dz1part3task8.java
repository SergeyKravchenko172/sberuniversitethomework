package dz1part3;

import java.util.Scanner;

public class Dz1part3task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int p = input.nextInt();
        int sum = 0, i = 0;
        do {
            int a = input.nextInt();
            if (a > p) {
                sum = sum + a;
            }
            i++;
        } while (i < n);
        System.out.println(sum);
    }
}
