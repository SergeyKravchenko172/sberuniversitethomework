package dz1part3;

import java.util.Scanner;

public class Dz1part3task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int numberOfNegativ = 0;
        int a = input.nextInt();
        while (a < 0) {
            numberOfNegativ = numberOfNegativ + 1;
            a = input.nextInt();
        }
        System.out.println(numberOfNegativ);
    }
}
