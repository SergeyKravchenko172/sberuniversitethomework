package dz1part3;

import java.util.Scanner;

public class Dz1part3task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();
        int raznica = 0;
        while (m >= n) {
            raznica = m - n;
            m = raznica;
        }
        System.out.println(m);
    }
}
