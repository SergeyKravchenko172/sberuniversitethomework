package dz1part3;

import java.util.Scanner;

public class Dz1part3task10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x = input.nextInt();
        for (int i = 0; i < x; i++) {
            for (int j = 1; j < x; j++) {
                if (j < x - i) {
                    System.out.print(" ");
                } else System.out.print("*");
            }
            for (int j = x; j > 0; j--) {
                if (j < x - i) {
                    System.out.print("");
                } else System.out.print("*");
            }
            System.out.println("");
        }
        for (int i = 1; i <= x; i++) {
            if (i == x) {
                System.out.print("|");
            } else System.out.print(" ");
        }
    }
}
