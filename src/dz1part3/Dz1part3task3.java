package dz1part3;

import java.util.Scanner;

public class Dz1part3task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();
        int rezultat = 0;
        for (int i = 1; i <= n; i++) {
            rezultat = rezultat + (int) Math.pow(m, i);
        }
        System.out.println(rezultat);
    }
}
