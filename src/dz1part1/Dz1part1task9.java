package dz1part1;

import java.util.Scanner;

public class Dz1part1task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double budget = input.nextDouble();
        double budgetPerGuest = input.nextDouble();
        int numberOfGuest = (int) (budget / budgetPerGuest);
        System.out.println(numberOfGuest);
    }
}
