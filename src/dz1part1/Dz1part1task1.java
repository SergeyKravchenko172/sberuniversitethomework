package dz1part1;

import java.util.Scanner;

public class Dz1part1task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double r = input.nextDouble();
        System.out.println(4.0 / 3 * Math.PI * Math.pow(r, 3));
    }
}
