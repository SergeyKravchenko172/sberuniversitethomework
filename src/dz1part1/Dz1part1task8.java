package dz1part1;

import java.util.Scanner;

public class Dz1part1task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double budget = input.nextDouble();
        int dayInMonth = 30;
        System.out.println(budget / dayInMonth);
    }
}
