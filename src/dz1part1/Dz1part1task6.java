package dz1part1;

import java.util.Scanner;

public class Dz1part1task6 {
    public static void main(String[] args) {
        final double kmToMile = 1.60934;
        Scanner input = new Scanner(System.in);
        double mile = input.nextDouble();
        System.out.println(mile / kmToMile);
    }
}
