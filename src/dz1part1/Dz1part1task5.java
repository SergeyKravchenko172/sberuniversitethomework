package dz1part1;

import java.util.Scanner;

public class Dz1part1task5 {
    public static void main(String[] args) {
        final double smToInch = 2.54;
        Scanner input = new Scanner(System.in);
        double inch = input.nextDouble();
        System.out.println(inch * smToInch);
    }
}
