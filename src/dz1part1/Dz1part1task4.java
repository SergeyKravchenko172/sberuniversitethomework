package dz1part1;

import java.util.Scanner;

public class Dz1part1task4 {
    public static void main(String[] args) {
        final int secondsInMinutes = 60;
        final int secondsInHoure = 3600;
        Scanner input = new Scanner(System.in);
        int count = input.nextInt();
        int hours = count / secondsInHoure;
        int minutes = count % secondsInHoure / secondsInMinutes;
        System.out.println(hours + " " + minutes);
    }
}
