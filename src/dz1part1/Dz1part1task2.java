package dz1part1;

import java.util.Scanner;

public class Dz1part1task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        System.out.println(Math.sqrt(1.0 / 2 * (a * a + b * b)));
    }
}
