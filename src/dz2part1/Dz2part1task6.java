package dz2part1;

import java.util.Scanner;

public class Dz2part1task6 {
public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    String string = scanner.nextLine();
    while (!string.matches("^[(А-Я)]*$")) {
        string = scanner.nextLine();
    }
    System.out.println(toMorzeCode(string));

}

public static String toMorzeCode(String str) {
    Object[] morzeCode = new Object[]{".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..",
            ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....",
            "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
    String result = new String();
    int numberANSICharRusA = (int) 'А';
    for (int i = 0; i < str.length(); i++) {
        int ch = (int) str.charAt(i);
        result = result + morzeCode[ch - numberANSICharRusA] + " ";
    }
    result = result.substring(0, result.length() - 1);
    return result;
}
}
