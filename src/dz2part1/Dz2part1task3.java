package dz2part1;

import java.util.Scanner;

public class Dz2part1task3 {
private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    int[] arr1 = inputArray();
    int x = scanner.nextInt();
    System.out.println(displayPositionOnArrayToPasteX(arr1, x));
}

public static int[] inputArray() {
    int n = scanner.nextInt();
    int[] array = new int[n];
    for (int i = 0; i < n; i++) {
        array[i] = scanner.nextInt();
    }
    return array;
}

public static int displayPositionOnArrayToPasteX(int[] array, int key) {
    int low = 0;
    int high = array.length - 1;
    int mid = 0;
    while (high >= low) {
        mid = (-low + high) / 2 + low;
        if (key < array[mid])
            high = mid - 1;
        else
            low = mid + 1;
    }
    return mid;
}
}
