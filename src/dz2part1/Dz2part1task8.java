package dz2part1;

import java.util.Scanner;

public class Dz2part1task8 {
private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    int[] arr1 = inputArray();
    int x = scanner.nextInt();
    maxNearInArray(arr1, x);
}

public static int[] inputArray() {
    int n = scanner.nextInt();
    int[] array = new int[n];
    for (int i = 0; i < n; i++) {
        array[i] = scanner.nextInt();
    }
    return array;
}

public static void maxNearInArray(int[] arr, int key) {
    int index = 0;
    int count = Math.abs(arr[index] - key);
    if (arr.length != 1) {
        for (int i = 1; i < arr.length; i++) {
            if ((count >= Math.abs(arr[i] - key)) && (arr[index] < arr[i])) {
                index = i;
                count = Math.abs(arr[i] - key);
            }
        }
    }
    System.out.println(arr[index]);
}
}
