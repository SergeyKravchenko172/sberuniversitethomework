package dz2part1;

import java.util.Arrays;
import java.util.Scanner;

public class Dz2part1task7 {


public static void main(String[] args) {
    int[] arr1 = inputArray();
    arrayElementsToSquare(arr1);
    Arrays.sort(arr1);
    displayArray(arr1);
}

public static int[] inputArray() {
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    int[] array = new int[n];
    for (int i = 0; i < n; i++) {
        array[i] = scanner.nextInt();
    }
    return array;
}

public static int[] arrayElementsToSquare(int[] arr) {
    for (int i = 0; i < arr.length; i++) {
        arr[i] *= arr[i];
    }
    return arr;
}


public static void displayArray(int[] arr) {
    for (int a : arr
    ) {
        System.out.print(a + " ");
    }
}

}
