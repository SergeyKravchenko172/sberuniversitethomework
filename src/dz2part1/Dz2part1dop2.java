package dz2part1;

import java.util.*;

public class Dz2part1dop2 {
public static void main(String[] args) {
//    int[] arr1 = inputArray();
//    int[] arr1={-10,-5,1,3,3,8};
//    arrayElementsToSquare(arr1);
    int n = 10_000_000;
    int[] arr1 = new int[n];
    for (int x = 0; x < arr1.length; x++) {
        arr1[x] = (int) (Math.random() * 1000000);
    }
    int[] arr2 = Arrays.copyOf(arr1, arr1.length);
    int[] arr3 = Arrays.copyOf(arr1, arr1.length);
    int[] arr4 = Arrays.copyOf(arr1, arr1.length);
    Date start = new Date();
    sort(arr1);
    Date finish = new Date();
    Date start2 = new Date();
    sortRaz1(arr2);
    Date finish2 = new Date();
    Date start3 = new Date();
    arr3 = sortRaz2(arr3);
    Date finish3 = new Date();
    Date start4 = new Date();
    sortRaz3(arr4);
    Date finish4 = new Date();
    long time1 = finish.getTime() - start.getTime();
    long time2 = finish2.getTime() - start2.getTime();
    long time3 = finish3.getTime() - start3.getTime();
    long time4 = finish4.getTime() - start4.getTime();
//    displayArray(arr1);
//    System.out.println();
//    displayArray(arr2);
//    System.out.println();
//    displayArray(arr3);
//    System.out.println();
//    displayArray(arr4);
//    System.out.println();
    System.out.println(time1);
    System.out.println(time2);
    System.out.println(time3);
    System.out.println(time4);
}

public static int[] inputArray() {
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    int[] array = new int[n];
    for (int i = 0; i < n; i++) {
        array[i] = scanner.nextInt();
    }
    return array;
}

public static int[] arrayElementsToSquare(int[] arr) {
    for (int i = 0; i < arr.length; i++) {
        arr[i] *= arr[i];
    }
    return arr;
}

public static int[] sort(int[] array) {
    int min, max;
    max = min = array[0];
    for (int i = 1; i < array.length; i++) {
        if (array[i] < min) {
            min = array[i];
        }
        if (array[i] > max) {
            max = array[i];
        }
    }
    return sort(array, min, max);
}

static int[] sort(int[] array, int min, int max) {
    int[] count = new int[max - min + 1];
    for (int i = 0; i < array.length; i++) {
        count[array[i] - min]++;
    }
    int idx = 0;
    for (int i = 0; i < count.length; i++) {
        for (int j = 0; j < count[i]; j++) {
            array[idx++] = i + min;
        }
    }
    return array;
}

//реализация через иф
public static int[] sortIf(int[] array) {
    int min, max;
    max = min = array[0];
    for (int i = 1; i < array.length; i++) {
        if (array[i] < min) {
            min = array[i];
        }
        if (array[i] > max) {
            max = array[i];
        }
    }
    int[] counts = new int[max + 1];
    for (int num : array) {
        counts[num]++;
    }
//    int[] sortedArray = new int[array.length];
    int currentSortedIndex = 0;
    for (int i = 0; i < counts.length; i++) {
        if (counts[i] > 0) {
            array[currentSortedIndex] = i;
            currentSortedIndex++;
            counts[i]--;
            i--;
        }
    }
    return array;
}
//конец реализации через иф


public static void displayArray(int[] arr) {
    for (int a : arr
    ) {
        System.out.print(a + " ");
    }
}


//1 реализация
// Main Radix Sort sort function
public static void sortRaz1(int A[]) {
    int digitPlace = 1;
    int n = A.length;
    int result[] = new int[n]; // resulting array
    // Find the largest number to know number of digits
    int largestNum = getMax(A);


    //we run loop until we reach the largest digit place
    while (largestNum / digitPlace > 0) {

        int count[] = new int[10];
        //Initializing counting array C[] to 0
        for (int i = 0; i < 10; i++)
            count[i] = 0;

        //Store the count of "keys" or digits in count[]
        for (int i = 0; i < n; i++)
            count[(A[i] / digitPlace) % 10]++;

        // Change count[i] so that count[i] now contains actual
        //  position of this digit in result[]
        //  Working similar to the counting sort algorithm
        for (int i = 1; i < 10; i++)
            count[i] += count[i - 1];

        // Build the resulting array
        for (int i = n - 1; i >= 0; i--) {
            result[count[(A[i] / digitPlace) % 10] - 1] = A[i];
            count[(A[i] / digitPlace) % 10]--;
        }

        // Now main array A[] contains sorted
        // numbers according to current digit place
        for (int i = 0; i < n; i++)
            A[i] = result[i];

        // Move to next digit place
        digitPlace *= 10;
    }
}

public static int getMax(int A[]) {
    int max = A[0];
    for (int i = 1; i < A.length; i++) {
        if (A[i] > max)
            max = A[i];
    }
    return max;
}

//конец 1 реализации
//2 реализация через методы
public static int[] sortRaz2(int[] data) {
    int maxLength = getMaxLength(data);
    int[] tmp = baseSort(data, 0, maxLength);
    return tmp;
}

/**
 * @param data
 * @param i
 * @param maxLength
 * @return
 */
private static int[] baseSort(int[] data, int i, int maxLength) {
    if (i >= maxLength) {
        return data;
    }
    int len = data.length;

    // Создаем 10 корзин, каждая корзина используется для хранения количества цифр от 0 до 9
    // Например, i = 0, count [1], он используется для хранения количества цифр, что каждый бит равен 1
    int[] count = new int[10];
    // Используется для копирования массива, вспомогательная сортировка
    int[] tmp = new int[len];

    // Помещаем все числа в массив в один и тот же вид согласно правилам
    for (int j = 0; j < tmp.length; j++) {
        count[getNum(data[j], i)]++;
    }

    // Число count [] представляет количество цифр в корзине и становится нижним индексом
    // Например: count [0] изначально было 1, count [1] было 1, затем следующая цифра count [1] будет count [0] + count [1] = 2
    for (int j = 1; j < count.length; j++) {
        count[j] = count[j - 1] + count[j];
    }
    // Копируем все элементы исходного массива в новый массив по порядку
    for (int j = tmp.length - 1; j >= 0; j--) {
        int number = data[j];
        int a = getNum(number, i);
        tmp[count[a] - 1] = number;
        count[a]--;
    }
    return baseSort(tmp, i + 1, maxLength);
}


/**
 * Получить i-ю цифру числа, единица начинаются с 0
 *
 * @param num
 * @param i
 * @return
 */
private static int getNum(int num, int i) {
    for (int j = 0; j < i + 1; j++) {
        if (j == i) {
            num %= 10;
        } else {
            num = num / 10;
        }
    }
    return num;
}

/**
 * Получить длину массива
 *
 * @param num
 * @return
 */
private static int length(int num) {
    return String.valueOf(num).length();
}

/**
 * Найдите максимальную длину всех элементов в массиве
 *
 * @param data
 * @return
 */
private static int getMaxLength(int[] data) {
    int maxLength = 0;
    for (int i = 0; i < data.length; i++) {
        if (maxLength < length(data[i])) {
            maxLength = length(data[i]);
        }
    }
    return maxLength;
}

//конец второй реализации
//3 реализация через аррайлист.
public static int[] sortRaz3(int[] array) {
    List<Integer>[] buckets = new ArrayList[10];
    for (int i = 0; i < buckets.length; i++) {
        buckets[i] = new ArrayList<>();
    }
    // sort
    boolean flag = false;
    int tmp = -1, divisor = 1;
    while (!flag) {
        flag = true;
        for (Integer i : array) {
            tmp = i / divisor;
            buckets[tmp % 10].add(i);
            if (flag && tmp > 0) {
                flag = false;
            }
        }
        int a = 0;
        for (int b = 0; b < 10; b++) {
            for (Integer i : buckets[b]) {
                array[a++] = i;
            }
            buckets[b].clear();
        }
        divisor *= 10;
    }
    return array;

}


//конец 3 реализации
}