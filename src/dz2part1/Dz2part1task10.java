package dz2part1;

import java.util.Scanner;

public class Dz2part1task10 {
public static void main(String[] args) {
    game();
}

public static void game() {
    int m = (int) (Math.random() * 1000);
    Scanner scanner = new Scanner(System.in);
    int x;
    do {
        System.out.print("Введите число: ");
        x = scanner.nextInt();
        if (x < 0) return;
        if (x == m) {
            System.out.println("Победа!");
            return;
        } else if (x < m) {
            System.out.println("Это число меньше загаданного.");

        } else System.out.println("Это число больше загаданного.");
    } while (x != m || x > 0);
}
}
