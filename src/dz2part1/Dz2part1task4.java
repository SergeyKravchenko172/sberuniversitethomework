package dz2part1;

import java.util.Scanner;

public class Dz2part1task4 {

public static void main(String[] args) {
    int[] arr1 = inputArray();
    displayNamberOfElementsAndSameElemet(arr1);
}

public static int[] inputArray() {
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    int[] array = new int[n];
    for (int i = 0; i < n; i++) {
        array[i] = scanner.nextInt();
    }
    return array;
}

public static void displayNamberOfElementsAndSameElemet(int[] arr) {
    int namberOfElements = 1;
    for (int i = 0; i < arr.length - 1; i++) {

        if (arr[i] == arr[i + 1]) {
            namberOfElements++;
        } else {
            System.out.println(namberOfElements + " " + arr[i]);
            namberOfElements = 1;
        }
    }
    System.out.println(namberOfElements + " " + arr[arr.length - 1]);
}
}
