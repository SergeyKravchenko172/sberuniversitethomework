package dz2part1;

import java.util.Scanner;

public class Dz2part1task5 {
private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    int[] arr1 = inputArray();
    int x = scanner.nextInt();
    shiftRightOnXElementsOfArray(arr1, x);
    displayArray(arr1);
}

public static int[] inputArray() {
    int n = scanner.nextInt();
    int[] array = new int[n];
    for (int i = 0; i < n; i++) {
        array[i] = scanner.nextInt();
    }
    return array;
}

public static void shiftRightOnXElementsOfArray(int[] arr, int key) {
    key = key % arr.length;
    if (key != 0) {
        int[] arrTemp = new int[key];
        System.arraycopy(arr, arr.length - key, arrTemp, 0, key);
        System.arraycopy(arr, 0, arr, key, arr.length - key);
        System.arraycopy(arrTemp, 0, arr, 0, key);
    }
}

public static void displayArray(int[] arr) {
    for (int a : arr
    ) {
        System.out.print(a + " ");
    }
}
}

