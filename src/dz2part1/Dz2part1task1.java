package dz2part1;

import java.util.Scanner;

public class Dz2part1task1 {
public static void main(String[] args) {
    double[] arr = inputArray();
    System.out.println(averageArifmeticArray(arr));
}

public static double[] inputArray() {
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    double[] array = new double[n];
    for (int i = 0; i < n; i++) {
        array[i] = scanner.nextDouble();
    }
    return array;
}

public static double averageArifmeticArray(double[] arr) {
    double result = 0;
    for (int i = 0; i < arr.length; i++) {
        result = result + arr[i];
    }
    result = result / arr.length;
    return result;
}
}

