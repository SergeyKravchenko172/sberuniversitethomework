package dz2part1;

import java.util.Scanner;

public class Dz2part1task2 {
private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    int[] arr1 = inputArray();
    int[] arr2 = inputArray();
    if (arr1.length == arr2.length) {
        boolean flag = true;
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                flag = false;
                break;
            }
        }
        System.out.println(flag);
    } else System.out.println(false);
}

public static int[] inputArray() {
    int n = scanner.nextInt();
    int[] array = new int[n];
    for (int i = 0; i < n; i++) {
        array[i] = scanner.nextInt();
    }
    return array;
}

}
