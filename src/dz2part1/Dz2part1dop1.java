package dz2part1;

import java.util.Scanner;

public class Dz2part1dop1 {
public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int n;
    do {
        n = scanner.nextInt();
        if (n < 8) {
            System.out.println("Пароль с " + n + " количеством символов небезопасен");
        }
    }
    while (n < 8);

    int nymbers = (int) Math.ceil(n / 8.);
    int specialSimbols = (int) Math.ceil(n / 8.);
    int charsUpperCase = (int) Math.round((n - nymbers - specialSimbols) / 2.);
    String password = new String();
    int x;
    for (int i = 0; i < n; i++) {
        boolean createSymbolPassword = false;
        do {
            x = (int) (Math.random() * 4);
            if (x == 0 && charsUpperCase > 0) {
                x = (int) (Math.random() * ('Z' - 'A'));
                password = password + (char) (x + (int) 'A');
                charsUpperCase--;
                createSymbolPassword = true;
            } else if (x == 1 && specialSimbols > 0) {
                x = (int) (Math.random() * (3));
                if (x == 0)
                    password = password + '_';
                else if (x == 1)
                    password = password + '-';
                else
                    password = password + '*';
                specialSimbols--;
                createSymbolPassword = true;
            } else if (x == 2 && nymbers > 0) {
                x = (int) (Math.random() * ('9' - '0'));
                password = password + (char) (x + (int) '0');
                nymbers--;
                createSymbolPassword = true;
            } else {
                x = (int) (Math.random() * ('z' - 'a'));
                password = password + (char) (x + (int) 'a');
                createSymbolPassword = true;
            }
        } while (!createSymbolPassword);
    }
    System.out.println(password);
}
}

