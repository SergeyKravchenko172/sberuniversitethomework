package dz2part1;

import java.util.Scanner;

public class Dz2part1task9 {
private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    String[] arr1 = inputArray();
    // displayArray(arr1);
    machElementOfArray(arr1);
}

public static String[] inputArray() {
    int n = scanner.nextInt();
    String[] array = new String[n];
    for (int i = 0; i < n; i++) {
        boolean flag = true;
        do {
            flag = false;
            array[i] = scanner.nextLine();
            if (array[i] == null || array[i].length() == 0) flag = true;
            else {
                for (int j = 0; j < array[i].length(); j++) {
                    if ((array[i].charAt(j) < 'a') && (array[i].charAt(j) > 'z')) {
                        flag = true;
                        break;
                    }
                }
            }
        }
        while (flag);


    }
    return array;
}

public static void machElementOfArray(String[] arr) {
    for (int i = 0; i < arr.length - 1; i++) {
        for (int j = i + 1; j < arr.length; j++) {
            if (arr[i].equalsIgnoreCase(arr[j])) {
                System.out.println(arr[i]);
                return;
            }
        }
    }
}

public static void displayArray(String[] arr) {
    for (String a : arr) {
        System.out.println(a);
    }
}
}
