package dz2part2;

import java.util.Scanner;

public class Dz2part2task5 {
private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    int[][] arr1 = input2xMatrix();
    System.out.println(simmetrySecondaryDiagonale(arr1));
}

public static int[][] input2xMatrix() {
    int n = scanner.nextInt();
    int[][] array = new int[n][n];
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            array[i][j] = scanner.nextInt();
        }
    }
    return array;
}

public static boolean simmetrySecondaryDiagonale(int[][] arr) {
    boolean result = true;
    for (int i = 0; i < arr.length; i++) {
        for (int j = 0; j < arr.length; j++) {
            if (arr[i][j] != arr[arr.length - j - 1][arr.length - i - 1]) result = false;
        }
    }

    return result;
}
}
