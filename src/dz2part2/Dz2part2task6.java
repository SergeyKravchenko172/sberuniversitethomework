package dz2part2;

import java.util.Scanner;

public class Dz2part2task6 {
private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    int[][] arr1 = input2xArray();
    dietNormCheck(arr1);
}

public static int[][] input2xArray() {
    int n = 4;
    int m = 8;
    int[][] array = new int[m][n];
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            array[i][j] = scanner.nextInt();
        }
    }
    return array;
}

public static void dietNormCheck(int[][] arr) {
    int summ;
    boolean chekDietnorm = true;
    for (int j = 0; j < arr[j].length; j++) {
        summ = 0;
        for (int i = 1; i < arr.length; i++) {
            summ = summ + arr[i][j];
        }
        if (summ > arr[0][j]) {
            chekDietnorm = false;
        }
    }
    if (chekDietnorm) System.out.println("Отлично");
    else System.out.println("Нужно есть поменьше");
}
}
