package dz2part2;

import java.util.Scanner;

public class Dz2part2task7 {
private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    int participant = scanner.nextInt();
    scanner.nextLine();
    String[] nameParticipant = inputArray(participant);
    String[] namePet = inputArray(participant);
    int[][] judgesScore = input2xArray(participant);
    printVinners(nameParticipant, namePet, judgesScore);
}

public static String[] inputArray(int n) {
    String[] array = new String[n];
    for (int i = 0; i < n; i++) {
        array[i] = scanner.nextLine();
    }
    return array;
}

public static int[][] input2xArray(int n) {
    int m = 3;
    int[][] array = new int[n][m];
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            array[i][j] = scanner.nextInt();
        }
    }
    return array;
}

public static void printVinners(String[] name, String[] pet, int[][] score) {
    int[] resultIndex = new int[3];
    double[] result = new double[3];
    for (int i = 0; i < score.length; i++) {
        double summ = 0;
        for (int j = 0; j < score[i].length; j++) {
            summ = summ + score[i][j];
        }
        summ = ((int) (summ * 10) / score[i].length) / 10.;
        if (summ > result[2]) {
            if (summ > result[1]) {
                if (summ > result[0]) {
                    result[2] = result[1];
                    result[1] = result[0];
                    result[0] = summ;
                    resultIndex[2] = resultIndex[1];
                    resultIndex[1] = resultIndex[0];
                    resultIndex[0] = i;
                } else {
                    result[2] = result[1];
                    result[1] = summ;
                    resultIndex[2] = resultIndex[1];
                    resultIndex[1] = i;
                }
            } else {
                result[2] = summ;
                resultIndex[2] = i;
            }
        }
    }
    for (int i = 0; i < result.length; i++) {
        System.out.println(name[resultIndex[i]] + ": " + pet[resultIndex[i]] + ", " + result[i]);

    }
}
}
