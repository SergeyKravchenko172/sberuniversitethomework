package dz2part2;

import java.util.Scanner;

public class Dz2part2task10 {
public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int a = scanner.nextInt();
    printDigitsRightToLeft(a);
}

public static void printDigitsRightToLeft(int a) {
    if (a / 10 == 0) {
        System.out.println(a + " ");
        return;
    }
    int result = a % 10;
    System.out.print(result + " ");
    printDigitsRightToLeft(a / 10);
}
}
