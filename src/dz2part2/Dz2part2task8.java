package dz2part2;

import java.util.Scanner;

public class Dz2part2task8 {
public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int a = scanner.nextInt();
    System.out.println(summDigitsOfNumber(a));
}

public static int summDigitsOfNumber(int a) {
    if (a / 10 == 0) return a;
    int result = a % 10 + summDigitsOfNumber(a / 10);
    return result;
}
}
