package dz2part2;

import java.util.Scanner;

public class Dz2part2task9 {
public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int a = scanner.nextInt();
    printDigitsLeftToRight(a);
}

public static void printDigitsLeftToRight(int a) {
    if (a / 10 == 0) {
        System.out.print(a + " ");
        return;
    }
    int result = a % 10;
    printDigitsLeftToRight(a / 10);
    System.out.print(result + " ");

}
}
