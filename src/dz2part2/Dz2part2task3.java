package dz2part2;

import java.util.Scanner;

public class Dz2part2task3 {

private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    int n = scanner.nextInt();
    char[][] array = new char[n][n];
    chessHorseInSuareMatrix(array);
}

public static void chessHorseInSuareMatrix(char[][] arr) {
    for (int i = 0; i < arr.length; i++) {
        for (int j = 0; j < arr[i].length; j++) {
            arr[i][j] = '0';
        }
    }
    int x = scanner.nextInt();
    int y = scanner.nextInt();
    arr[y][x] = 'K';
    for (int i = y - 2; i <= y + 2; i++) {
        for (int j = x - 2; j <= x + 2; j++) {
            if (i >= 0 && i < arr.length && j >= 0 && j < arr.length) {
                if (Math.abs(i - y) + Math.abs(j - x) == 3) arr[i][j] = 'X';
            }
        }

    }
    displayArray(arr);
}

public static void displayArray(char[][] arr) {
    for (int i = 0; i < arr.length; i++) {
        for (int j = 0; j < arr[i].length; j++) {
            if (j < arr[i].length - 1) System.out.print(arr[i][j] + " ");
            else System.out.print(arr[i][j]);
        }
        System.out.println();
    }
}
}

