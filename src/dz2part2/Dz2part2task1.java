package dz2part2;

import java.util.Scanner;

public class Dz2part2task1 {
private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    int[][] arr1 = input2xArray();
    displayArray(minInRowIn2xArray(arr1));
}

public static int[][] input2xArray() {
    int n = scanner.nextInt();
    int m = scanner.nextInt();
    int[][] array = new int[m][n];
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            array[i][j] = scanner.nextInt();
        }
    }
    return array;
}

public static int[] minInRowIn2xArray(int[][] arr) {
    int[] result = new int[arr.length];
    for (int i = 0; i < arr.length; i++) {
        result[i] = arr[i][0];
        for (int j = 1; j < arr[i].length; j++) {
            if (result[i] > arr[i][j]) result[i] = arr[i][j];
        }

    }
    return result;
}

public static void displayArray(int[] arr) {
    for (int a : arr
    ) {
        System.out.print(a + " ");
    }
}
}
