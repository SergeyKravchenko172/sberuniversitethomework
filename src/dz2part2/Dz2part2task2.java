package dz2part2;

import java.util.Scanner;

public class Dz2part2task2 {
private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    int n = scanner.nextInt();
    int[][] array = new int[n][n];
    squareInArray(array);
}

public static void squareInArray(int[][] arr) {
    int x1 = scanner.nextInt();
    int y1 = scanner.nextInt();
    int x2 = scanner.nextInt();
    int y2 = scanner.nextInt();
    for (int i = y1; i <= y2; i++) {
        for (int j = x1; j <= x2; j++) {
            if (j == x1 || j == x2) {
                arr[i][j] = 1;
            }
            if (i == y1 || i == y2) {
                arr[i][j] = 1;
            }
        }
    }
    displayArray(arr);
}

public static void displayArray(int[][] arr) {
    for (int i = 0; i < arr.length; i++) {
        for (int j = 0; j < arr[i].length; j++) {
            if (j < arr[i].length - 1) System.out.print(arr[i][j] + " ");
            else System.out.print(arr[i][j]);
        }
        System.out.println();
    }
}
}
