package dz2part2;

import java.util.Scanner;

public class Dz2part2task4 {
private static final Scanner scanner = new Scanner(System.in);

public static void main(String[] args) {
    int[][] arr1 = input2xMatrix();
    displayArray(deleteOneRowColumByElement(arr1));

}

public static int[][] input2xMatrix() {
    int n = scanner.nextInt();
    int[][] array = new int[n][n];
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            array[i][j] = scanner.nextInt();
        }
    }
    return array;
}

public static int[][] deleteOneRowColumByElement(int[][] arr) {
    int[][] result = new int[arr.length - 1][arr.length - 1];
    int indexi = 0, indexj = 0;
    int x = scanner.nextInt();
    for (int i = 0; i < arr.length; i++) {
        for (int j = 0; j < arr.length; j++) {
            if (arr[i][j] == x) {
                indexi = i;
                indexj = j;
            }
        }
    }
    for (int i = 0; i < arr.length; i++) {
        for (int j = 0; j < arr.length; j++) {
            if (i < indexi && j < indexj) {
                result[i][j] = arr[i][j];
            } else if (i > indexi && j < indexj) {
                result[i - 1][j] = arr[i][j];
            } else if (i > indexi && j > indexj) {
                result[i - 1][j - 1] = arr[i][j];
            }
            if (i < indexi && j > indexj) {
                result[i][j - 1] = arr[i][j];
            }

        }
    }
    return result;
}

public static void displayArray(int[][] arr) {
    for (int i = 0; i < arr.length; i++) {
        for (int j = 0; j < arr[i].length; j++) {
            if (j < arr[i].length - 1) System.out.print(arr[i][j] + " ");
            else System.out.print(arr[i][j]);
        }
        System.out.println();
    }
}
}
