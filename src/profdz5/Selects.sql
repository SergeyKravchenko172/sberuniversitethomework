--первый запрос: По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
select *
from orders
         join bayers b on b.id = orders.bayer_id
where orders.id = 1;
--второй запрос: Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
select *
from orders o
         join bayers b on b.id = o.bayer_id
where b.id = 3
  and time_stamp >= date_trunc('month', now() - interval '1 month')
  and time_stamp >= date_trunc('day', now() - interval '1 month');
--третий запрос: Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
select name, count
from orders o
         join flowers f on f.id = o.flower_id
where count = (select max(count) from orders);
--чевертый запрос: Вывести общую выручку (сумму золотых монет по всем заказам) за все время
select sum(order_price) AS Total
from orders;