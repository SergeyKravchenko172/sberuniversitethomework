insert into flowers(name, price)
values ('Роза', '100');
insert into flowers(name, price)
values ('Лилия', '50');
insert into flowers(name, price)
values ('Ромашка', '25');
insert into flowers(name, price)
values ('Кактус в горшочке малый', '250');
insert into flowers(name, price)
values ('Кактус в горшочке большой', '1000');

insert into bayers(name, number_phone)
values ('Роза', '8-921-555-0120');
insert into bayers(name, number_phone)
values ('Михаил', '8-960-555-4565');
insert into bayers(name, number_phone)
values ('Илья', '8-999-555-8273');

insert into orders(flower_id, bayer_id, count)
values (1, 2, 5);
insert into orders(flower_id, bayer_id, count)
values (4, 1, 3);
insert into orders(flower_id, bayer_id, count)
values (5, 1, 1);
insert into orders(flower_id, bayer_id, count, time_stamp)
values (2, 3, 3, TIMESTAMP '2021-02-16 15:36:38');
insert into orders(flower_id, bayer_id, count, time_stamp)
values (3, 3, 10, TIMESTAMP '2022-10-16 15:36:38');
insert into orders(flower_id, bayer_id, count, time_stamp)
values (1, 3, 3, TIMESTAMP '2020-12-16 15:36:38');
insert into orders(flower_id, bayer_id, count, time_stamp)
values (4, 1, 5, TIMESTAMP '2022-01-16 15:36:38');
insert into orders(flower_id, bayer_id, count, time_stamp)
values (5, 3, 2, TIMESTAMP '2020-08-16 15:36:38');
insert into orders(flower_id, bayer_id, count, time_stamp)
values (1, 3, 7, TIMESTAMP '2021-03-16 15:36:38');
insert into orders(flower_id, bayer_id, count, time_stamp)
values (2, 3, 8, TIMESTAMP '2022-11-02 15:36:38');