create table flowers
(
    id    serial primary key,
    name  varchar(50) not null,
    price integer     not null
);
create table bayers
(
    id           serial primary key,
    name         varchar(50) not null,
    number_phone varchar(15) not null
);
CREATE OR REPLACE FUNCTION flower_prise(count integer, idx integer) RETURNS integer AS
$$
BEGIN
    RETURN count * (select DISTINCT price
                    from flowers
                    where id = idx);
END;
$$ LANGUAGE plpgsql IMMUTABLE;

create table orders
(
    id          serial primary key,
    flower_id   integer REFERENCES flowers,
    bayer_id    integer REFERENCES bayers,
    count       integer not null check (count >= 1 and count <= 1000),
    order_price integer GENERATED ALWAYS AS ( flower_prise(count, flower_id)) Stored,
    time_stamp  timestamp default now()
);

drop table orders;