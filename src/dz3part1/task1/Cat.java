package dz3part1.task1;

public class Cat {
public void status() {
    int status = (int) (Math.random() * 3);
    switch (status) {
        case 0 -> sleep();
        case 1 -> meow();
        case 2 -> eat();
    }
}

private void sleep() {
    System.out.println("Sleep");
}

private void meow() {
    System.out.println("Meow");
}

private void eat() {
    System.out.println("Eat");
}
}
