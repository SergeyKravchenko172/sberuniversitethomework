package dz3part1.task2;

import java.util.Arrays;

public class Student {
private String name;
private String surname;
private int size = 0;
private int[] grades = new int[INITIAL_GRADES_LENGTH];
private static final int INITIAL_GRADES_LENGTH = 10;

public double averageGrades() {
    double result = 0;
    for (int i = 0; i < size; i++) {
        result = result + grades[i];
    }
    result = (int) (result / size * 100) / 100.;
    return result;
}

public void setGrades(int[] grades) {
    if (this.grades.length < grades.length) {
        for (int i = 0; i < this.grades.length; i++) {
            this.grades[i] = grades[grades.length - this.grades.length + i];
        }
    } else this.grades = Arrays.copyOf(grades, this.grades.length);
    this.size = grades.length;
}

public void setGrade(int grade) {
    if (size < grades.length) {
        grades[size] = grade;
        this.size++;
    } else {
        for (int i = 1; i < grades.length; i++) {
            grades[i - 1] = grades[i];
        }
        grades[size - 1] = grade;
    }
}

public String getName() {
    return name;
}

public void setName(String name) {
    this.name = name;
}

public String getSurname() {
    return surname;
}

public void setSurname(String surname) {
    this.surname = surname;
}

public int[] getGrades() {
    return grades;
}


}
