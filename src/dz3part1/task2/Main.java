package dz3part1.task2;

import java.util.Arrays;

public class Main {
public static void main(String[] args) {
    Student student = new Student();
    student.setName("Кирилл");
    student.setSurname("Васильев");
    System.out.println(Arrays.toString(student.getGrades()));
    student.setGrades(new int[]{5, 4, 3, 5, 5, 5});
//    student.setGrade(5);
//    student.setGrade(4);
//    student.setGrade(3);
//    student.setGrade(2);
//    student.setGrade(1);
//    student.setGrade(5);
//    student.setGrade(4);
//    student.setGrade(3);
    System.out.println(Arrays.toString(student.getGrades()));
    System.out.println(student.getName());
    System.out.println(student.getSurname());
    System.out.println(student.averageGrades());

}
}
