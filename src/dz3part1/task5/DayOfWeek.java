package dz3part1.task5;

public class DayOfWeek {
private byte dayNumber;
private String dayName;

public DayOfWeek(byte newDayNumber, String newDayName) {
    this.dayNumber = newDayNumber;
    this.dayName = newDayName;
}

public byte getDayNumber() {
    return dayNumber;
}

public String getDayName() {
    return dayName;
}
}
