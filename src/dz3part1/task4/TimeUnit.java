package dz3part1.task4;

public class TimeUnit {
private int houre;
private int minutes;
private int seconds;

public TimeUnit() {
    this.houre = 0;
    this.minutes = 0;
    this.seconds = 0;
}

public TimeUnit(int houre) {
    if (houre < 0 || houre > 23) {
        throw new IllegalArgumentException("\nВведенные значения времени некорректны.\nДиапазон значений часов: 0-13.\nДиапазон значений минут: 0-59.\nДиапазон значений секунд: 0-59.");
    }
    this.houre = houre;
    this.minutes = 0;
    this.seconds = 0;
}

public TimeUnit(int houre, int minutes) {
    if (houre < 0 || houre > 23 || minutes < 0 || minutes > 59) {
        throw new IllegalArgumentException("\nВведенные значения времени некорректны.\nДиапазон значений часов: 0-13.\nДиапазон значений минут: 0-59.\nДиапазон значений секунд: 0-59.");
    }
    this.houre = houre;
    this.minutes = minutes;
    this.seconds = 0;
}

public TimeUnit(int houre, int minutes, int seconds) {
    if (houre < 0 || houre > 23 || minutes < 0 || minutes > 59 || seconds < 0 || seconds > 59) {
        throw new IllegalArgumentException("\nВведенные значения времени некорректны.\nДиапазон значений часов: 0-13.\nДиапазон значений минут: 0-59.\nДиапазон значений секунд: 0-59.");
    }
    this.houre = houre;
    this.minutes = minutes;
    this.seconds = seconds;
}


public void plusTime(int houre, int minutes, int seconds) {
    if (houre < 0 || minutes < 0 || minutes > 59 || seconds < 0 || seconds > 59) {
        throw new IllegalArgumentException("\nВведенные значения времени некорректны.\nДиапазон значений часов: должно быть положительным.\nДиапазон значений минут: 0-59.\nДиапазон значений секунд: 0-59.");
    }
    if (this.seconds + seconds >= 60) {
        this.minutes = this.minutes + (this.seconds + seconds) / 60;
        this.seconds = (this.seconds + seconds) % 60;
    } else this.seconds = this.seconds + seconds;
    if (this.minutes + minutes >= 60) {
        this.houre = this.houre + (this.minutes + minutes) / 60;
        this.minutes = (this.minutes + minutes) % 60;
    } else this.minutes = this.minutes + minutes;
    if (this.houre + houre >= 24) {
        this.houre = (this.houre + houre) % 24;
    } else this.houre = this.houre + houre;
}

public void display24() {
    System.out.println((houre < 10 ? "0" + houre : houre) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds));
}

public void display12() {
    if (houre / 12 == 0)
        System.out.println((houre < 10 ? "0" + houre : houre) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds) + " AM");
    else
        System.out.println((houre < 10 ? "0" + houre : houre) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds) + " PM");
}
}
