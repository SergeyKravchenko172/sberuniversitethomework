package dz3part1.task4;

public class Main {
public static void main(String[] args) {
    TimeUnit timeUnit = new TimeUnit(11, 13, 21);
    TimeUnit timeUnit2 = new TimeUnit(23, 25);
    TimeUnit timeUnit3 = new TimeUnit(3);
    timeUnit.display24();

    timeUnit.plusTime(3, 15, 36);
    timeUnit.display24();
    timeUnit2.display12();
    timeUnit2.plusTime(0, 37, 55);
    timeUnit2.display24();
}
}
