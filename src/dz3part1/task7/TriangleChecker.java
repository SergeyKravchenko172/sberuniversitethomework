package dz3part1.task7;

public class TriangleChecker {
public static boolean Check(double a, double b, double c) {
    if (a <= 0 || b <= 0 || c <= 0) return false;
    double x = Math.max(a, Math.max(b, c));
    if (x < (a + b + c - x)) {
        return true;
    } else {
        return false;
    }
}
}
