package dz3part1.task7;

public class Main {
public static void main(String[] args) {
    System.out.println(TriangleChecker.Check(5, 6, 12));
    System.out.println(TriangleChecker.Check(12, 6, 6));
    System.out.println(TriangleChecker.Check(7, 12, 6));
    System.out.println(TriangleChecker.Check(5.5, 6.9, 12.1));
    System.out.println(TriangleChecker.Check(54.6, 6, 16));
    System.out.println(TriangleChecker.Check(12.98, 60, 72.3));
    System.out.println(TriangleChecker.Check(-12.98, 60, 72.3));
    System.out.println(TriangleChecker.Check(12.98, -60, 72.3));
    System.out.println(TriangleChecker.Check(12.98, 60, -72.3));
    System.out.println(TriangleChecker.Check(12.98, 60, 0));
}
}
