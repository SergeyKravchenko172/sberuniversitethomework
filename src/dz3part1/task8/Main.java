package dz3part1.task8;

public class Main {
public static void main(String[] args) {
    Atm a1 = new Atm(0.045, 10);
    Atm a2 = new Atm(-12.65, 10);
    Atm a3 = new Atm(12.65, 10);
    Atm b1 = new Atm(0.019, 52.5);
    Atm b2 = new Atm(12.65, 100);
    Atm b3 = new Atm(-3, 25);
    System.out.println(a1.convertRubToUsd(100));
    System.out.println(a2.convertRubToUsd(100));
    System.out.println(a3.convertRubToUsd(-100));
    System.out.println(b1.convertUsdToRub(100));
    System.out.println(b2.convertUsdToRub(-100));
    System.out.println(b3.convertUsdToRub(0));
    System.out.println(Atm.getUsageAt());
}
}
