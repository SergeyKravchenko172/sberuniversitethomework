package dz3part1.task8;

public class Atm {
private double rubToUsd;
private double usdToRub;


private static int usageAtm = 0;

public static int getUsageAt() {
    return usageAtm;
}

public Atm(double rubToUsd, double usdToRub) {
    this.rubToUsd = rubToUsd;
    this.usdToRub = usdToRub;
    usageAtm++;
}


public int convertRubToUsd(int rub) {
    if (rub >= 0) {
        return (int) (rub * rubToUsd);
    } else throw new IndexOutOfBoundsException("\nКоличество рублей должно быть положительным");
}

public int convertUsdToRub(int usd) {
    if (usd >= 0) {
        return (int) (usd * usdToRub);
    } else throw new IndexOutOfBoundsException("\nКоличество долларов должно быть положительным");
}

}
