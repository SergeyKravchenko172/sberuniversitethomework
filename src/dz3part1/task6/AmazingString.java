package dz3part1.task6;

public class AmazingString {
private char[] chars;

public int getLenght() {
    return lenght;
}

private int lenght;

public AmazingString(String string) {
    this.lenght = string.length();
    this.chars = new char[lenght];
    for (int i = 0; i < lenght; i++) {
        this.chars[i] = string.charAt(i);
    }
}

public AmazingString(char[] chars) {
    this.chars = chars;
    this.lenght = chars.length;
}

public int lenght() {
    if (chars == null) {
        throw new NullPointerException("\nМассив не был инициирован");
    }
    return lenght;
}

public char charAt(int x) {
    if (lenght == 0) {
        throw new IndexOutOfBoundsException("\nМассив с 0 размером не содержит символов");
    }
    if (x >= 0 && x < lenght) return chars[x];
    else {
        throw new IndexOutOfBoundsException("\nВведите значение индекса в диапазоне: 0-" + (this.lenght - 1));
    }
}

public void displayString() {
    if (chars == null) {
        throw new NullPointerException("\nМассив не был инициирован");
    } else {
        for (int i = 0; i < lenght; i++) {
            System.out.print(chars[i]);
        }
        System.out.println();
    }
}

public boolean containSubString(char[] subChars) {
    if (subChars == null) return false;
    boolean rezult = false;
    int i = 0;
    while (!rezult && i < this.lenght - subChars.length) {
        for (int j = 0; j < subChars.length; j++) {
            if (chars[i + j] == subChars[j]) {
                rezult = true;
            } else {
                rezult = false;
                break;
            }
        }
        i++;
    }
    return rezult;
}

public boolean containSubString(String subString) {
    if (subString == null) return false;
    AmazingString subChars = new AmazingString(subString);
    boolean rezult = false;
    int i = 0;
    char temp;
    while (!rezult && i < lenght - subChars.getLenght()) {
        for (int j = 0; j < subChars.getLenght(); j++) {
            temp = subChars.charAt(j);
            if (chars[i + j] == temp) {
                rezult = true;
            } else {
                rezult = false;
                break;
            }
        }
        i++;
    }
    return rezult;
}

public void delLeadingSpase() {
    int indx = 0;
    if (lenght == 0 || chars == null) return;
    else {
        while (chars[indx] == ' ') {
            indx++;
        }
        this.lenght = lenght - indx;
        char[] rezult = new char[lenght];
        for (int i = 0; i < lenght; i++) {
            rezult[i] = chars[i + indx];
        }
        this.chars = rezult;
    }
}

public void reverse() {
    if (lenght == 0 || chars == null) return;
    else {
        char[] rezult = new char[lenght];
        for (int i = 0; i < lenght; i++) {
            rezult[i] = chars[lenght - i - 1];
        }
        this.chars = rezult;
    }
}

}
