package dz3part1.task6;

public class Main {
public static void main(String[] args) {
    AmazingString as1 = new AmazingString("");
    char[] ch = {' ', 'f', 'd', 'g'};
    char[] ch2 = {'d', 'g'};
    AmazingString t = new AmazingString("qwerty");
    System.out.println(t.containSubString(new char[]{'q', 'r'}));
    AmazingString as2 = new AmazingString(ch);
    AmazingString as3 = new AmazingString("fe   wvbqoevdgqe");
    as1.displayString();
    as2.displayString();
    as3.displayString();
    System.out.println(as2.containSubString(ch2));
    System.out.println(as2.containSubString("dg"));
    System.out.println(as3.containSubString(ch2));
    System.out.println(as3.containSubString("wvbqoe"));
    System.out.println(as3.containSubString("fe "));
    System.out.println(as1.charAt(0));
    System.out.println(as2.charAt(2));
    System.out.println(as3.charAt(2));
    as1.reverse();
    as2.reverse();
    as3.reverse();
    as1.displayString();
    as2.displayString();
    as3.displayString();
    as2.delLeadingSpase();
    as2.displayString();
}
}
