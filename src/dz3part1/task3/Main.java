package dz3part1.task3;

import java.util.Arrays;

public class Main {
public static void main(String[] args) {
    Student[] students = new Student[5];
    students[0] = new Student("Алексей", "Петров", new int[]{5, 4, 4, 5, 5, 5, 3, 3, 3});
    students[1] = new Student("Алексей", "Иванов", new int[]{5, 4, 3, 5, 5, 5});
    students[2] = new Student("Алексей", "Шариков", new int[]{5, 5, 5});
    students[3] = new Student("Николай", "Шариков", new int[]{5, 5, 5, 5, 1});
    students[4] = new Student("Алексей", "Шаров", new int[]{5, 4, 5, 5});
    Student[] students2 = new Student[5];
    students2[0] = new Student("Алексей", "Петров", new int[]{5, 4, 4, 5, 3, 3, 3});
    students2[1] = new Student("Алексей", "Иванов", new int[]{5, 4, 3, 5, 5, 5});
    students2[2] = new Student("Алексей", "Шариков", new int[]{5, 4, 5});
    students2[3] = new Student("Николай", "Шариков", new int[]{5, 5, 5, 5, 1});
    students2[4] = new Student("Алексей", "Шаров", new int[]{5, 4, 5, 5});

    StudentService studentService = new StudentService(students);
    studentService.displayStudents();
    System.out.println();
    Student best = studentService.bestStudent();
    System.out.println(Arrays.toString(best.getGrades()));
    System.out.println(best.getName());
    System.out.println(best.getSurname());
    System.out.println(best.averageGrades());
    System.out.println();
    best = studentService.bestStudent(students2);
    System.out.println(Arrays.toString(best.getGrades()));
    System.out.println(best.getName());
    System.out.println(best.getSurname());
    System.out.println(best.averageGrades());
    System.out.println();
    studentService.sortBySurname();
    studentService.displayStudents();
    System.out.println();
    studentService = new StudentService(studentService.sortBySurname(students2));
    studentService.displayStudents();
}
}
