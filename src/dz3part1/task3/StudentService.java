package dz3part1.task3;

import java.util.Arrays;

public class StudentService {
Student[] students;
private int size;
private static final int SIZE_INITIAL = 5;

public StudentService() {
    this.students = new Student[SIZE_INITIAL];
    this.size = 0;
}

public StudentService(Student[] students) {
    this.size = students.length;
    this.students = students;
}

public void setStudent() {
    Student newStudent = new Student();
    if (size >= students.length) {
        students = Arrays.copyOf(students, (students.length * 2));
    }
    students[size] = newStudent;
    this.size++;
}

public void setStudent(Student newStudent) {
    if (size >= students.length) {
        students = Arrays.copyOf(students, (students.length * 2));
    }
    students[size] = newStudent;
    size++;
}

public Student bestStudent() { //рандомный студент из лучших.
    double best = 0;
    int numbersOfBest = 0;
    for (int i = 0; i < size; i++) {
        if (best == students[i].averageGrades()) {
            numbersOfBest++;
        } else if (best < students[i].averageGrades()) {
            best = students[i].averageGrades();
            numbersOfBest = 1;
        }
    }
    Student[] bestsStudents = new Student[numbersOfBest];
    int j = 0;
    for (
            int i = 0;
            i < size; i++) {
        if (best == students[i].averageGrades()) {
            bestsStudents[j] = students[i];
            j++;
        }
    }

    int position = (int) (Math.random() * numbersOfBest);
    return bestsStudents[position];
}

public Student bestStudent(Student[] groupStudents) {
    double best = 0;
    int numbersOfBest = 0;
    for (int i = 0; i < size; i++) {
        if (best == groupStudents[i].averageGrades()) {
            numbersOfBest++;
        } else if (best < groupStudents[i].averageGrades()) {
            best = groupStudents[i].averageGrades();
            numbersOfBest = 1;
        }
    }

    Student[] bestsStudents = new Student[numbersOfBest];
    int j = 0;
    for (int i = 0; i < size; i++) {
        if (best == groupStudents[i].averageGrades()) {
            bestsStudents[j] = groupStudents[i];
            j++;
        }
    }
    int position = (int) (Math.random() * numbersOfBest);
    return bestsStudents[position];
}

public Student[] sortBySurname() {
    Student tempStudent;
    int x = 1;
    do {
        for (int i = x; i < size; i++) {

            for (int j = 0; j < students[i - 1].getSurname().length() && j < students[i].getSurname().length(); j++) {
                if ((int) students[i - 1].getSurname().charAt(j) > (int) students[i].getSurname().charAt(j)) {
                    tempStudent = students[i - 1];
                    students[i - 1] = students[i];
                    students[i] = tempStudent;
                    break;
                } else if (students[i - 1].getSurname().charAt(j) < students[i].getSurname().charAt(j)) {
                    break;
                } //если буквы равны сравниваем следующие
            }
        }
        x++;
    }
    while (x < students.length);
    return students;
}

public Student[] sortBySurname(Student[] groupStudents) {
    Student tempStudent;
    int x = 1;
    do {
        for (int i = x; i < size; i++) {

            for (int j = 0; j < groupStudents[i - 1].getSurname().length() && j < groupStudents[i].getSurname().length(); j++) {
                if ((int) groupStudents[i - 1].getSurname().charAt(j) > (int) groupStudents[i].getSurname().charAt(j)) {
                    tempStudent = groupStudents[i - 1];
                    groupStudents[i - 1] = groupStudents[i];
                    groupStudents[i] = tempStudent;
                    break;
                } else if (groupStudents[i - 1].getSurname().charAt(j) < groupStudents[i].getSurname().charAt(j)) {
                    break;
                } //если буквы равны сравниваем следующие
            }
        }
        x++;
    }
    while (x < groupStudents.length);
    return groupStudents;

}


public void displayStudents() {
    for (int i = 0; i < size; i++) {
        System.out.println(students[i].getSurname() + ", " + students[i].getName() + ", " + students[i].averageGrades());
    }
}

public int getSize() {
    return size;
}
}