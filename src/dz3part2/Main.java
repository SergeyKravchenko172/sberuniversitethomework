package dz3part2;

import java.util.Arrays;

public class Main {
public static void main(String[] args) {
    Library library = new Library();
    library.setBook("AAA", "AAA", "AAA");
    library.setBook("AAB", "AAA", "AAA");
    library.setBook("AAC", "AAA", "AAA");
    library.setBook("BAA", "BBB", "BBB");
    library.setBook("AAA", "BBB", "BBB");
    library.setBook("CAA", "CCC", "CCC");
    library.setBook("CAB", "CCC", "CCD");
    for (int i = 0; i < library.books.size(); i++) {
        System.out.println(library.books.get(i) + " " + library.books.get(i).getTitle());
    }
    System.out.println(library.books.size());
    library.deleteBook("AAB");
    System.out.println(Arrays.toString(Book.titles.toArray()));
    System.out.println(library.getBook("AAA"));
    System.out.println(Arrays.toString(library.getBookFromAutor("AAA", "AAA")));

    library.setVisitors("ZZZ", "ZZZ");
    library.setVisitors("YYY", "YYY");
    library.setVisitors("YYY", "ZZZ");
    library.setVisitors("XXX", "XXX");
    library.setVisitors("XXX");
    library.setVisitors("VVV");

    library.takeBook("ZZZ", "ZZZ", "AAA");
    library.takeBook("YYY", "YYY", "BAA");
    library.takeBook("YYY", "YYY", "CAB");
    library.takeBook("VVV", "asd");
    library.takeBook("XXX", "CAA");
    for (int i = 0; i < library.visitors.size(); i++) {
        System.out.println(library.visitors.get(i).getIdReader() + " " + library.visitors.get(i).getName() + " " + library.visitors.get(i).getSurname() + " " + library.visitors.get(i).getBook());
    }
    library.returnBook("ZZZ", "AAA");
    library.returnBook("ZZZ", "BAA");
    library.returnBook("YYY", "YYY", "BAA");
    library.takeBook("VVV", "BAA");
    library.returnBook("VVV", "BAA");
    for (int i = 0; i < library.visitors.size(); i++) {
        System.out.println(library.visitors.get(i).getIdReader() + " " + library.visitors.get(i).getName() + " " + library.visitors.get(i).getSurname() + " " + library.visitors.get(i).getBook());
    }
    for (int i = 0; i < library.books.size(); i++) {
        System.out.println(library.books.get(i).getTitle() + " " + library.books.get(i).getStar());
    }


}
}
