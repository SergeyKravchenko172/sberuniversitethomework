package dz3part2;

import java.util.ArrayList;
import java.util.Scanner;

public class Library {
ArrayList<Book> books = new ArrayList<>();
ArrayList<Visitor> visitors = new ArrayList<>();
private static final Scanner scanner = new Scanner(System.in);

public void setBook(String title, String nameAutor, String surnameAutor) {
    if (Book.titles == null) Book.titles = new ArrayList<>();
    if (Book.titles.contains(title)) {
        System.out.println("Такая книга уже есть в библиотеке");
    } else {
        books.add(new Book(title, nameAutor, surnameAutor));
    }
}

public void deleteBook(String title) {
    if (!Book.titles.contains(title)) {
        System.out.println("Такой книги не было в библиотеке.");
    } else {
        int x = Book.titles.indexOf(title);
        books.remove(x);
        Book.titles.remove(x);
    }
}

public Book getBook(String title) {
    if (!Book.titles.contains(title)) {
        System.out.println("Такой книги не числится в библиотеке.");
        return null;
    } else {
        int x = Book.titles.indexOf(title);
        return books.get(x);
    }
}

public Book[] getBookFromAutor(String name, String surname) {
    int size = 0;
    for (int i = 0; i < books.size() - 1; i++) {
        if (books.get(i).getNameAutor().equals(name) && books.get(i).getSurnameAutor().equals(surname)) {
            size++;
        }
    }
    if (size == 0) {
        System.out.println("Книг " + name + " " + surname + " нет в библиотеке.");
        return null;
    } else {
        Book[] rezult = new Book[size];
        int j = 0;
        for (int i = 0; i < books.size() - 1; i++) {
            if (books.get(i).getNameAutor().equals(name) && books.get(i).getSurnameAutor().equals(surname)) {
                rezult[j] = books.get(i);
                j++;
            }
        }
        return rezult;
    }
}

public void setVisitors(String name, String surname) {
    if (visitors == null) {
        visitors = new ArrayList<>();
    }
    visitors.add(new Visitor(name, surname));
}

public void setVisitors(String name) {
    if (visitors == null) {
        visitors = new ArrayList<>();
    }
    visitors.add(new Visitor(name));
}

public void takeBook(String visitorName, String visitorSurname, String title) {
    Book book = getBook(title);
    if (book == null) {
        return;
    }
    if (!book.isAvailable()) {
        System.out.println("Книга уже взята читателем");
        return;
    }
    Visitor reader = oneReaderFromVisitors(visitorName, visitorSurname);
    if (reader == null) {
        return;
    }
    if (reader.getBook() == null) {
        reader.takeBook(book);
        book.takes();
    } else System.out.println("Книга уже взята читателем");
}

public void takeBook(String visitorName, String title) {
    Book book = getBook(title);
    if (book == null) {
        return;
    }
    if (!book.isAvailable()) {
        System.out.println("Книга уже взята читателем");
        return;
    }
    Visitor reader = oneReaderFromVisitors(visitorName);
    if (reader == null) {
        return;
    }
    if (reader.getBook() == null) {
        reader.takeBook(book);
        book.takes();
    } else System.out.println("Книга уже взята читателем");
}

private Visitor oneReaderFromVisitors(String name, String surname) {
    int indx = 0;
    for (int i = 0; i < visitors.size(); i++) {
        if (visitors.get(i).getName().equals(name) && visitors.get(i).getSurname().equals(surname))
            indx++;
    }
    Visitor rezult = null;
    if (indx != 0) {
        Visitor[] temp = new Visitor[indx];
        for (int i = 0; i < visitors.size(); i++) {
            if (visitors.get(i).getName().equals(name) && visitors.get(i).getSurname().equals(surname)) {
                temp[indx - 1] = visitors.get(i);
                indx--;
            }
        }
        return getVisitor(rezult, temp);
    } else {
        System.out.println("Таких посетителей не зарегистрировано");
        return rezult;
    }
}

private Visitor oneReaderFromVisitors(String name) {
    int indx = 0;
    for (int i = 0; i < visitors.size(); i++) {
        if (visitors.get(i).getName().equals(name))
            indx++;
    }
    Visitor rezult = null;
    if (indx != 0) {
        Visitor[] temp = new Visitor[indx];
        for (int i = 0; i < visitors.size(); i++) {
            if (visitors.get(i).getName().equals(name)) {
                temp[indx - 1] = visitors.get(i);
                indx--;
                if (indx == 0) break;
            }
        }
        return getVisitor(rezult, temp);
    } else {
        System.out.println("Таких посетителей не зарегистрировано");
        return rezult;
    }
}

private Visitor getVisitor(Visitor rezult, Visitor[] temp) {
    if (temp.length == 1) {
        rezult = temp[0];
        return rezult;
    } else {
        String spase = " ";
        while (rezult == null) {
            System.out.println("ID IDReader  Name  Surname");
            for (int i = 0; i < temp.length; i++) {
                System.out.println(temp[i].getIdVisitor() + spase + temp[i].getIdReader() + spase + temp[i].getName() + spase + temp[i].getSurname());
            }
            System.out.print("Выберете посетителя введя его ID: ");
            int sc = scanner.nextInt();
            for (int i = 0; i < temp.length; i++) {
                if (sc == temp[i].getIdVisitor()) {
                    rezult = temp[i];
                }
            }
        }
        return rezult;
    }
}

public void returnBook(String visitorName, String visitorSurname, String title) {
    Book book = getBook(title);
    if (book == null) {
        return;
    }
    Visitor reader = oneReaderFromVisitors(visitorName, visitorSurname);
    if (reader == null) {
        return;
    }
    if (!book.isAvailable() && reader.getBook() == book) {
        reader.returnBook(book);
        book.returns();
        setStars(book);
    } else System.out.println("Книга взята другим читателем.");
}

public void returnBook(String visitorName, String title) {
    Book book = getBook(title);
    if (book == null) {
        return;
    }
    Visitor reader = oneReaderFromVisitors(visitorName);
    if (reader == null) {
        return;
    }
    if (!book.isAvailable() && reader.getBook() == book) {
        reader.returnBook(book);
        book.returns();
        setStars(book);
    } else System.out.println("Книга взята другим читателем.");
}

public void setStars(Book book) {
    int star = -1;
    while (!(star >= 0 && star <= 10)) {
        System.out.print("Пожалуйста оцените книгу " + book.getTitle() + " (1 - совсем не понравилось, 10 - супер шедевр) 0-пропустить: ");
        star = scanner.nextInt();
    }
    if (star == 0) return;
    else {
        book.setStar(star);
    }
}
}
