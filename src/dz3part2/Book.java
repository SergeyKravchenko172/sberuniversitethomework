package dz3part2;

import java.util.ArrayList;

public class Book {
private String title;
public static ArrayList<String> titles;
private String nameAutor;
private String surnameAutor;
private boolean available;
private double star = 0;
private ArrayList<Integer> stars;

public Book(String title, String nameAutor, String surnameAutor) {
    titles.add(title);
    this.title = title;
    this.nameAutor = nameAutor;
    this.surnameAutor = surnameAutor;
    this.available = true;
}

public String getTitle() {
    return title;
}

public String getNameAutor() {
    return nameAutor;
}

public String getSurnameAutor() {
    return surnameAutor;
}

public boolean isAvailable() {
    return available;
}

public void returns() {
    this.available = true;
}

public void takes() {
    this.available = false;
}

public double getStar() {
    return star;
}

public void setStar(int star) {
    if (stars == null) stars = new ArrayList<>();
    stars.add(star);
    int summ = 0;
    for (Integer x : stars
    ) {
        summ = summ + x;

    }
    this.star = summ * 1.0 / stars.size();
}

}
