package dz3part2;

public class Visitor {
private String name;
private String surname;
private static int countVisitor;
private static int idReaders;
private int idReader;
private int idVisitor;
private Book getBook;

public Visitor(String name, String surname) {
    this.name = name;
    this.surname = surname;
    countVisitor++;
    this.idVisitor = countVisitor;
    this.getBook = null;
}

public Visitor(String name) {
    this.name = name;
    this.surname = null;
    countVisitor++;
    this.idVisitor = countVisitor;
    this.getBook = null;
}


public void takeBook(Book book) {
    if (idReader == 0) {
        idReaders++;
        this.idReader = idReaders;
    }
    this.getBook = book;
}

public void returnBook(Book book) {
    if (this.getBook == book) {
        this.getBook = null;
    } else System.out.println("Эа книга не у данного читателя");
}


public String getName() {
    return name;
}

public String getSurname() {
    return surname;
}

public int getIdVisitor() {
    return this.idVisitor;
}

public int getIdReader() {
    return idReader;
}

public void setSurname(String surname) {
    this.surname = surname;
}

public Book getBook() {
    return getBook;
}
}
