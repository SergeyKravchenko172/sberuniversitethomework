package profdz2.task1;

import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		ArrayList<Integer> arrInt = new ArrayList<>();
		for (int i = 0; i < 20; i++) {
			arrInt.add(i * 6 / 10);
		}
		System.out.println(arrInt);
		System.out.println(new UniqueElementsInList<Integer>().uniqueElementsInList(arrInt));
	}
}
