package profdz2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class UniqueElementsInList<T> {
	private ArrayList<T> uniqueElementsList = new ArrayList<>();

	public ArrayList<T> uniqueElementsInList(List<T> list) throws RuntimeException {
		if (list == null) throw new NullPointerException("Переданный в метод List = null");
		HashSet<T> hash = new HashSet<>();
		hash.addAll(list);
		uniqueElementsList.addAll(hash);
		return uniqueElementsList;
	}

}
