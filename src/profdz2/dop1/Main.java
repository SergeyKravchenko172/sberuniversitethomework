package profdz2.dop1;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) {
		String[] words = {"the", "day", "is", "sunny", "the", "the", "the", "is", "day", "day", "sunny", "sunny", "sgst"};
		System.out.println(Arrays.toString(words));
		int k = 4;
		String[] listWord = Arrays.stream(words)
				.filter(w -> w.length() >= 1)    //убираем из подсчета пустые сроки
				.collect(Collectors.toMap(key -> key, v -> 1, Integer::sum))      //переводим в MAP для подсчета количества слов
				.entrySet().stream()    //обратно делаем поток из значений мапы
				.sorted((e1, e2) -> {      //сортируем
					int sortValue = -1 * e1.getValue().compareTo(e2.getValue());      //по убываю количества слов, через сортировку Integer
					if (sortValue == 0)
						return e1.getKey().compareTo(e2.getKey());    //по лексикографическом порядку, через сортировку String
					return sortValue;
				})
				.limit(k)      //задаем лимит на первые k слов
				.map(Map.Entry::getKey).toArray(String[]::new);    //переводим в массив строк по условию задачи

		System.out.println(Arrays.toString(listWord));
	}
}
