package profdz2.task3;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {
	public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
		if (set1 == null || set2 == null) throw new NullPointerException("Переданный в метод set1 или set2 = null");
		Set<T> rezult = new HashSet<>(set1);
		rezult.retainAll(set2);
		return rezult;
	}

	public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
		if (set1 == null || set2 == null) throw new NullPointerException("Переданный в метод set1 или set2 = null");
		Set<T> rezult = new HashSet<>(set1);
		rezult.addAll(set2);
		return rezult;
	}

	public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
		if (set1 == null || set2 == null) throw new NullPointerException("Переданный в метод set1 или set2 = null");
		Set<T> rezult = new HashSet<>(set1);
		rezult.removeAll(set2);
		return rezult;
	}
}
