package profdz2.task2;

import java.util.ArrayList;
import java.util.Collections;

public class IsAnnogramm {
	public static boolean isAnnogramm(String s, String t) throws RuntimeException {
		if (s == null || t == null) throw new NullPointerException("Одна из строк Null!");
		if (s.length() != t.length() || s.equals(t)) return false;
		ArrayList<Character> sChars = new ArrayList<>();
		for (int i = 0; i < s.length(); i++) {
			sChars.add(s.charAt(i));
		}
		ArrayList<Character> tChars = new ArrayList<>();
		for (int i = 0; i < t.length(); i++) {
			tChars.add(t.charAt(i));
		}
		Collections.sort(sChars);
		Collections.sort(tChars);
		return sChars.equals(tChars);

	}
}
