package profdz2.task4;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {
	public static void main(String[] args) {
		Document doc1 = new Document("AAA", 10);
		Document doc2 = new Document("BBB", 15);
		Document doc3 = new Document("CCC", 3);
		ArrayList<Document> archiveList = new ArrayList<>();
		archiveList.add(doc1);
		archiveList.add(doc2);
		archiveList.add(doc3);
		HashMap<Integer, Document> ArchiveMap = new HashMap<>(Archive.organizeDocuments(archiveList));
		System.out.println(ArchiveMap.get(2).toString());
	}
}
