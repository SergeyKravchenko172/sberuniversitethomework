package profdz2.task4;

public class Document {
	private static int documents = 0;
	private int id;
	private String name;
	private int pageCount;

	public Document(String name, int pageCount) {
		documents++;
		this.id = documents; //id начинается с 1! - номер документа удобный для понимания обычным пользователем.
		this.name = name;
		this.pageCount = pageCount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	@Override
	public String toString() {
		return "Document{" +
				"id=" + id +
				", name='" + name + '\'' +
				", pageCount=" + pageCount +
				'}';
	}

}
