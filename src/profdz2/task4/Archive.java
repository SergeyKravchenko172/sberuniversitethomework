package profdz2.task4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Archive {
	public static Map<Integer, Document> organizeDocuments(List<Document> documents) throws RuntimeException {
		if (documents == null) throw new NullPointerException("Лист = Null");
		HashMap<Integer, Document> hMap = new HashMap<>();
		for (Document document : documents) {
			hMap.put(document.getId(), document);
		}
		return hMap;
	}
}
