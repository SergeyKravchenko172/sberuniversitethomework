package profdz4.task6;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class Main {
	public static void main(String[] args) {
		Set<Integer> set1 = new HashSet<>();
		set1.add(1);
		set1.add(2);
		set1.add(3);
		Set<Integer> set2 = new HashSet<>();
		set2.add(4);
		set2.add(5);
		set2.add(6);
		Set<Integer> set3 = new HashSet<>();
		set3.add(6);
		set3.add(7);
		set3.add(8);
		Set<Set<Integer>> setSets = new LinkedHashSet<>();
		setSets.add(set1);
		setSets.add(set2);
		setSets.add(set3);

		System.out.println(setSets);
		Set<Integer> rezult = setSets
				.stream()
				.reduce((x, y) -> {
					x.addAll(y);
					return x;
				})
				.get();
		System.out.println(rezult);
	}
}
