package profdz4.task4;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		List<Double> list = new ArrayList<>();
		list.add(Math.random() * 100);
		list.add(Math.random() * 100);
		list.add(Math.random() * 100);
		list.add(Math.random() * 100);
		list.add(Math.random() * 100);
		System.out.println(list);

		List<Double> sortedList = list
				.stream()
				.sorted((o1, o2) -> -1 * Double.compare(o1, o2))
				.toList();
		System.out.println(sortedList);
	}

}
