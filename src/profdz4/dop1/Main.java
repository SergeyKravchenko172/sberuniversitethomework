package profdz4.dop1;

public class Main {
	public static void main(String[] args) {

		String s1 = "casat";
		String s2 = "cast";
		System.out.println(sravni(s1, s2));

	}

	public static boolean sravni(String lhs, String rhs) {
		if (lhs.length() - rhs.length() > 1 || lhs.length() - rhs.length() < -1) return false;
		if (lhs.length() - rhs.length() == 1) {
			for (int i = 0; i < rhs.length(); i++) {
				if (lhs.charAt(i) != rhs.charAt(i)) {
					return lhs.substring(i + 1, lhs.length() - 1).contains(rhs.substring(i, rhs.length() - 1));
				}
			}
			return true;
		}
		if (lhs.length() - rhs.length() == -1) {
			for (int i = 0; i < lhs.length(); i++) {
				if (lhs.charAt(i) != rhs.charAt(i)) {
					return rhs.substring(i + 1, rhs.length() - 1).contains(lhs.substring(i, lhs.length() - 1));
				}
			}
			return true;
		}
		int flag = 0;
		for (int i = 0; i < lhs.length(); i++) {
			if (lhs.charAt(i) != rhs.charAt(i)) flag++;
		}
		return flag <= 1;

	}
}