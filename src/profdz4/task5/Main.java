package profdz4.task5;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("asd");
		list.add("asdga");
		list.add("d");
		list.add("d");
		list.add("d");
		list.add("d");
		list.add("sd");

		System.out.println(list
				.stream()
				.map(String::toUpperCase)
//						.reduce((x,y)->x+", "+y));
				.collect(Collectors.joining(", ")));

	}
}
