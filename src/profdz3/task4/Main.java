package profdz3.task4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		Class<Chaild> cls = Chaild.class;
		List<Class<?>> rezult = allInterfaceOfClass(cls);
		for (Class<?> r : rezult
		) {
			System.out.println(r.getSimpleName());
		}
	}

	public static List<Class<?>> allInterfaceOfClass(Class<?> cls) {
		List<Class<?>> rezult = new ArrayList<>();
		while (cls != Object.class) {
			rezult.addAll(allInterfaceOfInteface(cls));
			cls = cls.getSuperclass();
		}
		return rezult;
	}

	public static List<Class<?>> allInterfaceOfInteface(Class<?> cls) {
		List<Class<?>> rezult = new ArrayList<>();
		List<Class<?>> intefaceses = Arrays.asList(cls.getInterfaces());
		if (intefaceses.size() == 0) return rezult;
		rezult.addAll(intefaceses);
		for (Class<?> aClass : intefaceses
		) {
			rezult.addAll(allInterfaceOfInteface(aClass));
		}
		return rezult;
	}
}
