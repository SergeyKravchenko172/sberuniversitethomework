package profdz3.dop1_2;

public class Chek {
	public boolean chek(String str) {
		if (str == null || str.length() % 2 == 1) return false;
		if (!(str.contains("()") || str.contains("{}") || str.contains("[]"))) return false;
		String rezult = str.replaceAll("[(][)]|[{][}]|[\\[][\\]]", "");
		if (rezult.length() == 0) return true;
		return chek(rezult);

	}
}
