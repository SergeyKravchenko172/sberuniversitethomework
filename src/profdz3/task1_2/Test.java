package profdz3.task1_2;

@IsLike
public class Test {
	String name = "Test Class";

	public Test(String name) {
		this.name = name;
		System.out.println("Создан Test класс с именем: " + this.name);
	}

	public Test() {
		System.out.println("Создан Test класс с именем: " + this.name);
	}
}
