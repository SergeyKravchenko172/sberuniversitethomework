package profdz3.task1_2;

public class Main {
	public static void main(String[] args) {
		metodContainsAnnotationIsLike(Test.class);
	}

	private static void metodContainsAnnotationIsLike(Class<?> cls) {
		if (cls.isAnnotationPresent(IsLike.class)) {
			IsLike isLike = cls.getAnnotation(IsLike.class);
			System.out.println(isLike.b);
		} else System.out.println("Класс не подписан аннотацией @IsLike");
	}
}
