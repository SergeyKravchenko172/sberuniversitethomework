package profdz1.dop1;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {
			int n = scanner.nextInt();
			int[] arr = new int[n];
			for (int i = 0; i < n; i++) {
				arr[i] = scanner.nextInt();
			}
			int max1 = Integer.MIN_VALUE, max2 = Integer.MIN_VALUE;
			for (int i = 0; i < n; i++) {
				if (arr[i] >= max1) {
					max2 = max1;
					max1 = arr[i];
				}
			}
			System.out.println(max1 + " " + max2);
		} catch (Exception e) {
			System.out.println("Ошибка");
		}
	}
}
