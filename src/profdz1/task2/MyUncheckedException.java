package profdz1.task2;

public class MyUncheckedException extends RuntimeException {
	public MyUncheckedException() {
		super("Непроверяемое исключение");
	}

	public MyUncheckedException(String message) {
		super(message);
	}
}
