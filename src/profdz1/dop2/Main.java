package profdz1.dop2;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {
			int n = scanner.nextInt();
			int[] arr = new int[n];
			for (int i = 0; i < n; i++) {
				arr[i] = scanner.nextInt();
			}
			int p = scanner.nextInt();
			System.out.println(findIndex(arr, p));
		} catch (Exception e) {
			System.out.println("Ошибка");
		}

	}

	public static int findIndex(int[] array, int x) {
		return findIndex(array, 0, array.length - 1, x);
	}

	public static int findIndex(int[] array, int left, int right, int x) {
		if (left > right) return -1;
		int middle = right - left / 2;
		if (x == array[middle]) {
			return middle;
		} else if (x < array[middle]) {
			return findIndex(array, left, middle - 1, x);
		} else return findIndex(array, middle + 1, right, x);

	}
}
