package profdz1.task4;

public class MyEvenNumber {
	private int number;

	public MyEvenNumber(int number) throws IllegalArgumentException {
		try {
			if (number / 2 != 0) {
				this.number = number;
			} else throw new NonEvenException();
		} catch (NonEvenException e) {
			System.out.println(e.getMessage());
		}
	}

	public int getNumber() {
		return number;
	}
}
