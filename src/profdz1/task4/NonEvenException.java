package profdz1.task4;

public class NonEvenException extends IllegalArgumentException {
	public NonEvenException() {
		super("Веденное число не является четным");
	}

}
