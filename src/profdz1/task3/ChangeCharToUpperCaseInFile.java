package profdz1.task3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

public class ChangeCharToUpperCaseInFile {
	private final String PKG_DIRECTORY = "D:/Education/JavaProjeckts/sberuniversitethomework/src/profdz1/task3";
	private final String OUTPUT_FILE_NAME = "output.txt";
	private final String INPUT_FILE_NAME = "input.txt";
	String inputPath = PKG_DIRECTORY + "/" + INPUT_FILE_NAME;
	String outputPath = PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME;

	public ChangeCharToUpperCaseInFile() {
		new ChangeCharToUpperCaseInFile(inputPath, outputPath);
	}

	public ChangeCharToUpperCaseInFile(String inputPath, String outputPath) {
		try (Scanner scanner = new Scanner(new File(inputPath));
		     Writer writer = new FileWriter(outputPath)) {
			while (scanner.hasNext()) {
				String str = scanner.nextLine();
				char c;
				for (int i = 0; i < str.length(); i++) {
					if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
						c = (char) (str.charAt(i) + ('A' - 'a'));
					} else c = str.charAt(i);

					writer.write(c);
				}
				writer.write("\n");

			}
		} catch (IOException e) {
			System.out.println("ошибка доступа к файлам");
		}

	}
}
