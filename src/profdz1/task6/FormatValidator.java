package profdz1.task6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;

public class FormatValidator {
	public void checkName(String str) throws RuntimeException {
		if (str == null) {
			throw new NullPointerException();
		}
		if (str.length() < 2 || str.length() > 20 || (!((str.charAt(0) >= 'A' && str.charAt(0) <= 'Z')) && !((str.charAt(0) >= 'А' && str.charAt(0) <= 'Я')))) {
			throw new FormatException();

		}
		System.out.println("Все ок");
	}

	public void checkBirthdate(String str) throws RuntimeException {
		if (str == null) {
			throw new NullPointerException();
		}
		String dateFormat = "dd.MM.uuuu";
		String dateString = "27.02.1989";
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter
				.ofPattern(dateFormat)
				.withResolverStyle(ResolverStyle.STRICT);
		try {
			LocalDate date = LocalDate.parse(str, dateTimeFormatter);
			LocalDate dateBegin = LocalDate.parse("01.01.1900", dateTimeFormatter);
			LocalDate dateNow = LocalDate.now();
			if (date.isBefore(dateBegin) || date.isAfter(dateNow)) {
				throw new FormatException();
			}

		} catch (DateTimeParseException e) {
			System.out.println("Ахтунг!");
		}
		System.out.println("Все ок");
	}

	public void checkGender(String str) throws RuntimeException {
		if (str == null) {
			throw new NullPointerException();
		}
		boolean b = true;
		for (Gender e : Gender.values()) {

			if (str.equals(e.name())) {
				b = false;
			}
		}
		if (b) {
			throw new FormatException();
		}
		System.out.println("Все ок");
	}

	public void checkHeight(String str) throws RuntimeException {
		if (str == null) {
			throw new NullPointerException();
		}
		if (!str.matches("[012]{1}(\\.)(\\d){2}") && (!str.matches("[012]{1}\\ м\\ \\d{2}\\ см"))) {
			throw new FormatException();
		}
		System.out.println("Все ок");
	}
}

