package profdz1.task1;

public class MyCheckedException extends Exception {
	public MyCheckedException() {
		super("Непроверяемое исключение");
	}

	public MyCheckedException(String message) {
		super(message);
	}
}
