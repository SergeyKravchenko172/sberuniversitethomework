package dz3part3.task3;

import java.util.Scanner;

public class Main {
	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		int n = scanner.nextInt();
		int m = scanner.nextInt();
		Matrix matrix = new Matrix(n, m);
		matrix.display();

	}
}
