package dz3part3.task3;

import java.util.ArrayList;

public class Matrix {
	ArrayList<ArrayList<Integer>> matrix;

	public Matrix(int colums, int row) {
		this.matrix = new ArrayList<>();
		for (int i = 0; i < row; i++) {
			matrix.add(new ArrayList<>());
			for (int j = 0; j < colums; j++) {
				matrix.get(i).add(i + j);
			}

		}
	}

	public void display() {
		for (int i = 0; i < matrix.size(); i++) {
			for (int j = 0; j < matrix.get(i).size(); j++) {
				if (j < matrix.get(i).size() - 1) System.out.print(matrix.get(i).get(j) + " ");
				else System.out.print(matrix.get(i).get(j));
			}
			System.out.println();
		}

	}
}
