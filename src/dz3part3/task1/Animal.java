package dz3part3.task1;

public abstract class Animal {
	protected final static void eat() {
		System.out.println("Кушает");
	}

	protected final static void sleep() {
		System.out.println("Спит");
	}
}
