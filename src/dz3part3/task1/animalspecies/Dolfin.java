package dz3part3.task1.animalspecies;

import dz3part3.task1.Mammal;
import dz3part3.task1.interfase.Swiming;

public class Dolfin extends Mammal implements Swiming {
	@Override
	public void swim() {
		System.out.println("Плавает быстро");
	}
}
