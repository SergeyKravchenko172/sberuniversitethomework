package dz3part3.task1.animalspecies;

import dz3part3.task1.Mammal;
import dz3part3.task1.interfase.Flying;

public class Bat extends Mammal implements Flying {
	@Override
	public void fly() {
		System.out.println("Летает медленно");
	}

}
