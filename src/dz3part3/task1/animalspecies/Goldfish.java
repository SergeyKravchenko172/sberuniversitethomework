package dz3part3.task1.animalspecies;

import dz3part3.task1.Fish;
import dz3part3.task1.interfase.Swiming;

public class Goldfish extends Fish implements Swiming {
	@Override
	public void swim() {
		System.out.println("Плавает медленно");
	}

}
