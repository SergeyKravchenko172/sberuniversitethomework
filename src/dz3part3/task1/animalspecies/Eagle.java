package dz3part3.task1.animalspecies;

import dz3part3.task1.Bird;
import dz3part3.task1.interfase.Flying;

public class Eagle extends Bird implements Flying {
	@Override
	public void fly() {
		System.out.println("Летает быстро");
	}
}
