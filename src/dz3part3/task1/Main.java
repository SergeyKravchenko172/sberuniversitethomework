package dz3part3.task1;

import dz3part3.task1.animalspecies.Bat;
import dz3part3.task1.animalspecies.Dolfin;
import dz3part3.task1.animalspecies.Eagle;
import dz3part3.task1.animalspecies.Goldfish;

public class Main {
	public static void main(String[] args) {
		Bat bat = new Bat();
		Eagle eagle = new Eagle();
		Goldfish goldfish = new Goldfish();
		Dolfin dolfin = new Dolfin();
		bat.fly();
		bat.wayOfBirth();
		bat.sleep();
		bat.eat();
		System.out.println();
		eagle.fly();
		eagle.wayOfBirth();
		eagle.sleep();
		eagle.eat();
		System.out.println();
		goldfish.swim();
		goldfish.wayOfBirth();
		goldfish.sleep();
		goldfish.eat();
		System.out.println();
		dolfin.swim();
		dolfin.wayOfBirth();
		dolfin.sleep();
		dolfin.eat();
	}
}
