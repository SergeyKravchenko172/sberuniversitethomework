package dz3part3.task4;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		int participantsNumbers = scanner.nextInt();
		scanner.nextLine();
		ArrayList<Participant> participants = new ArrayList<>();
		ArrayList<Dog> dogs = new ArrayList<>();
		for (int i = 0; i < participantsNumbers; i++) {
			participants.add(new Participant(scanner.nextLine(), i));
		}
		for (int i = 0; i < participantsNumbers; i++) {
			dogs.add(new Dog(scanner.nextLine(), i));
		}
		Judges judges = new Judges(input2xArray(participantsNumbers));

//		for (int i = 0; i < participantsNumbers; i++) {
//			participants.add(new Participant(i));
//		}
//		for (int i = 0; i < participantsNumbers; i++) {
//			dogs.add(new Dog(i));
//		}
//		Judges judges = new Judges(participantsNumbers);
		/*Чем закоменитированый вариант хуже активного? Он работает, но стопориться при вводе всех входных данных разом, из-за нового сканера внутри конструктора*/

		for (int i = 0; i < judges.vinners().length; i++) {
			System.out.println(participants.get(judges.vinners()[i]).getName() + ": " + dogs.get(judges.vinners()[i]).getName() + ", " + judges.getJudgesAverageScore()[judges.vinners()[i]]);
		}

	}

	public static int[][] input2xArray(int n) {
		int m = 3;//число судей из условия
		int[][] array = new int[n][m];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				array[i][j] = scanner.nextInt();
			}
		}
		return array;
	}
}
