package dz3part3.task4;

import java.util.Scanner;

public class Judges {
	int[][] judgesScore;
	double[] score;
	private int numberOfJudges = 3;
	private static final Scanner scannerJ = new Scanner(System.in);

	public Judges(int participant) {
		this.judgesScore = input2xArray(participant);
	}

	public Judges(int participant, Scanner scanner) {
		this.judgesScore = input2xArray(participant, scanner);
	}

	public Judges(int[][] judgesScore) {
		this.numberOfJudges = judgesScore[0].length;
		this.judgesScore = judgesScore;
	}

	public Judges(int participant, int numberOfJudges) {
		this.numberOfJudges = numberOfJudges;
		this.judgesScore = input2xArray(participant);
	}

	public int[][] input2xArray(int n) {
		int m = numberOfJudges;
		int[][] array = new int[n][m];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				array[i][j] = scannerJ.nextInt();
			}
		}
		return array;
	}

	public int[][] input2xArray(int n, Scanner scanner) {
		int m = numberOfJudges;
		int[][] array = new int[n][m];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				array[i][j] = scanner.nextInt();
			}
		}
		return array;
	}

	public int[][] getJudgesScore() {
		return judgesScore;
	}

	public double[] getJudgesAverageScore() {
		double[] result = new double[judgesScore.length];
		for (int i = 0; i < judgesScore.length; i++) {
			double summ = 0;
			for (int j = 0; j < judgesScore[i].length; j++) {
				summ = summ + judgesScore[i][j];
			}
			summ = ((int) (summ * 10) / judgesScore[i].length) / 10.;
			result[i] = summ;
		}
		return result;
	}

	public int[] vinners() {
		double[] score = getJudgesAverageScore();
		int[] resultIndex = new int[3];
		double[] result = new double[3];
		for (int i = 0; i < score.length; i++) {
			if (score[i] > result[2]) {
				if (score[i] > result[1]) {
					if (score[i] > result[0]) {
						result[2] = result[1];
						result[1] = result[0];
						result[0] = score[i];
						resultIndex[2] = resultIndex[1];
						resultIndex[1] = resultIndex[0];
						resultIndex[0] = i;
					} else {
						result[2] = result[1];
						result[1] = score[i];
						resultIndex[2] = resultIndex[1];
						resultIndex[1] = i;
					}
				} else {
					result[2] = score[i];
					resultIndex[2] = i;
				}
			}
		}
		return resultIndex;
	}
}
