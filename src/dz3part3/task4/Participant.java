package dz3part3.task4;

import java.util.Scanner;

public class Participant {
	private final int indx;
	private final String name;
	private static final Scanner scannerP = new Scanner(System.in);

	public Participant(String name, int indx) {
		this.indx = indx;
		this.name = name;
	}

	public Participant(Scanner scanner, int indx) {
		this.indx = indx;
		this.name = scanner.nextLine();
	}

	public Participant(int indx) {
		this.indx = indx;
		this.name = scannerP.nextLine();
	}

	public int getIndx() {
		return indx;
	}

	public String getName() {
		return name;
	}
}
