package dz3part3.task4;

import java.util.Scanner;

public class Dog {
	private final int indx;
	private final String name;
	double grade;
	private static final Scanner scannerDog = new Scanner(System.in);

	public Dog(String name, int indx) {
		this.indx = indx;
		this.name = name;
	}

	public Dog(Scanner scanner, int indx) {
		this.indx = indx;
		this.name = scanner.nextLine();
	}

	public Dog(int indx) {
		this.indx = indx;
		this.name = scannerDog.nextLine();
		;
	}

	public int getIndx() {
		return indx;
	}

	public String getName() {
		return name;
	}

	public double getGrade() {
		return grade;
	}
}
