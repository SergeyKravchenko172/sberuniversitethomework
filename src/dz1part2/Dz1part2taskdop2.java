package dz1part2;

import java.util.Scanner;

public class Dz1part2taskdop2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String mailPackage = input.nextLine();
        int chek1 = 0, chek2 = 0;
        if (mailPackage.startsWith("камни!") || mailPackage.endsWith("камни!")) {
            chek1 = 1;
        }
        if (mailPackage.startsWith("запрещенная продукция") || mailPackage.endsWith("запрещенная продукция")) {
            chek2 = 10;
        }
        int x = chek1 + chek2;
        switch (x) {
            case 1: {
                System.out.println("камни в посылке");
                break;
            }
            case 10: {
                System.out.println("в посылке запрещенная продукция");
                break;
            }
            case 11: {
                System.out.println("в посылке камни и запрещенная продукция");
                break;
            }
            default: {
                System.out.println("все ок");
                break;
            }
        }
    }
}
