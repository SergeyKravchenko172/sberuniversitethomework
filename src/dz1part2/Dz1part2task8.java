package dz1part2;

import java.util.Scanner;

public class Dz1part2task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String string = input.nextLine();
        int x = string.lastIndexOf(' ');
        System.out.println(string.substring(0, x));
        System.out.println(string.substring(x + 1, string.length()));

    }
}
