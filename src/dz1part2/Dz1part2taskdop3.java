package dz1part2;

import java.util.Scanner;

public class Dz1part2taskdop3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String model = input.nextLine();
        int price = input.nextInt();
        int minPrice = 50000;
        int maxPrice = 120000;
        if ((model.contains("iphone") || model.contains("samsung")) && price >= minPrice && price <= maxPrice) {
            System.out.println("Можно купить");
        } else System.out.println("Не подходит");
    }
}
