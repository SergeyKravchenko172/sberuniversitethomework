package dz1part2;

import java.util.Scanner;

public class Dz1part2task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int correntHoure = input.nextInt();
        if (correntHoure < 13) System.out.println("Рано");
        else System.out.println("Пора");
    }
}
