package dz1part2;

import java.util.Scanner;

public class Dz1part2task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x = input.nextInt();
        int y = input.nextInt();
        if (x > 0 && y > 0) System.out.println(true);
        else System.out.println(false);
    }
}
