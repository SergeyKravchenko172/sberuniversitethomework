package dz1part2;

import java.util.Scanner;

public class Dz1part2task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x = input.nextInt();
        int vacation = 6;
        int dayBeforVacation = vacation - x;
        if (dayBeforVacation <= 0) System.out.println("Ура, выходные!");
        else System.out.println(dayBeforVacation);
    }
}
