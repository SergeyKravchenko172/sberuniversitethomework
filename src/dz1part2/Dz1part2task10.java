package dz1part2;

import java.util.Scanner;

public class Dz1part2task10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double n = input.nextDouble();
        String namber = (n + "");
        int nambersAfterDot = namber.length() - 1 - namber.indexOf('.');
        double scale = Math.pow(10, nambersAfterDot);
        if (Math.round(Math.log(Math.exp(n)) * scale) / scale == Math.round(n * scale) / scale) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }
}
