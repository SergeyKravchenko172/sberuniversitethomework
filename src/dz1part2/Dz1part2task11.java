package dz1part2;

import java.util.Scanner;

public class Dz1part2task11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        int x = Math.max(a, Math.max(b, c));
        if (x < (a + b + c - x)) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

    }
}
