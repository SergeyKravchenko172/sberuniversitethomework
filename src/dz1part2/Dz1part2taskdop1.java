package dz1part2;

import java.util.Scanner;

public class Dz1part2taskdop1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String string = input.nextLine();
        int check = 0;
        if (string.length() > 7) {
            for (int c = 0; c < string.length(); c++) {
                if (string.charAt(c) == '_' || string.charAt(c) == '*' || string.charAt(c) == '-') {
                    check++;
                    break;
                }
            }
            for (int c = 0; c < string.length(); c++) {
                if (string.charAt(c) >= 'A' && string.charAt(c) <= 'Z') {
                    check++;
                    break;
                }
            }
            for (int c = 0; c < string.length(); c++) {
                if (string.charAt(c) >= 'a' && string.charAt(c) <= 'z') {
                    check++;
                    break;
                }
            }
            for (int c = 0; c < string.length(); c++) {
                if (string.charAt(c) >= '0' && string.charAt(c) <= '9') {
                    check++;
                    break;
                }
            }
            check++;
        }
        if (check == 5) {
            System.out.println("пароль надежный");
        } else
            System.out.println("пароль не прошел проверку");

    }
}