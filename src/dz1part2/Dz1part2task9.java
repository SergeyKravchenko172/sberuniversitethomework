package dz1part2;

import java.util.Scanner;

public class Dz1part2task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double x = input.nextDouble();
        double xRadians = Math.toRadians(x);
        double cos2 = Math.pow(Math.cos(xRadians), 2);
        double sin2 = Math.pow(Math.sin(xRadians), 2);
        double scale = Math.pow(10, 5);
        double result = Math.round((cos2 + sin2) * scale) / scale;
        if (result - 1 == 0) System.out.println(true);
        else {
            System.out.println(false);
        }
    }
}

