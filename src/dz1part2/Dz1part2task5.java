package dz1part2;

import java.util.Scanner;

public class Dz1part2task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        int discriminant = ((int) Math.pow(b, 2) - 4 * a * c);
        if (discriminant >= 0) System.out.println("Решение есть");
        else System.out.println("Решения нет");
    }
}
